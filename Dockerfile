# Usa una imagen base de Maven y Java JDK con la versión que necesitas
FROM maven:3.6.0-jdk-11-slim AS build

# Establece el directorio de trabajo en /home/app
WORKDIR /home/app

# Copia el archivo pom.xml para descargar las dependencias
COPY pom.xml .

# Descarga las dependencias del proyecto
#RUN mvn dependency:go-offline

# Copia todo el código fuente al contenedor
COPY src src

# Empaqueta la aplicación
RUN mvn package

# Ahora, inicia una nueva etapa de construcción para el contenedor de aplicación
FROM openjdk:11-ea-9-jdk-slim

# Establece el directorio de trabajo en /usr/src/app
WORKDIR /usr/src/app

# Copia el archivo JAR de la aplicación al contenedor y renómbralo
COPY --from=build /home/app/target/proyecto_v1-0.0.1-SNAPSHOT.jar proyecto_v1-1.0.0.jar

# Exponer el puerto en el que la aplicación se ejecutará
EXPOSE 9090

# Ejecutar la aplicación Spring Boot al iniciar el contenedor
CMD ["java", "-Delastic.apm.service_name=myproject", "-Delastic.apm.application_packages=com.example.myproject", "-jar", "proyecto_v1-1.0.0.jar"]