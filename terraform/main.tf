
resource "aws_instance" "proyecto_v1_ec2" {
  ami                         = var.ami_id   # Utiliza la variable para la AMI ID
  instance_type               = "t2.micro"   # Tipo de instancia, ajusta según tus necesidades
  key_name                    = "key-victor" # Nombre de tu keypair en AWS
  subnet_id                   = aws_subnet.public_subnets.id
  vpc_security_group_ids      = [aws_security_group.proyecto_v1_ec2_sg.id]
  associate_public_ip_address = true # Asigna una dirección IP pública a la instancia

count = var.instance_count
  tags = {
    Name = "${var.instance_name}_ec2-${count.index+1}"
  }
}

resource "aws_security_group" "proyecto_v1_ec2_sg" {
  name        = "${var.instance_name}_ec2_sg"
  description = "Security group for EC2 instance"
  vpc_id      = aws_vpc.Main_Proyecto_v1.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

  ingress {
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.instance_name}_Security_Group"
  }
}


