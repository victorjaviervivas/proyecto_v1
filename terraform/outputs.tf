output "vpc_id" {
  description = "ID de la VPC creada"
  value       = aws_vpc.Main_Proyecto_v1.id
}

output "subnet_id" {
  description = "ID de la subred pública creada"
  value       = aws_subnet.public_subnets.id
}

output "private_subnet_id" {
  description = "ID de la subred privada creada"
  value       = aws_subnet.private_subnets.id
}

output "nat_gateway_id" {
  description = "ID del NAT Gateway creado"
  value       = aws_nat_gateway.NAT_GW.id
}

output "public_ip" {
  description = "Dirección IP pública de las instancias EC2"
  value       = [for instance in aws_instance.proyecto_v1_ec2 : instance.public_ip]
}

output "instance_ids" {
  description = "IDs de las instancias EC2 creadas"
  value       = aws_instance.proyecto_v1_ec2[*].id
}
