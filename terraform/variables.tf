# ==================================================================
# Proposito:    declaramos todas las variables que vamos a usar
# Autor:        Victor Javaier Vivas 
# Fecha:        16.04.2024
# Version:      1.0
# ==================================================================


variable "aws_region_id" {
  description = "la region"
  type        = string
  default     = "us-east-1"
}
variable "main_vpc_cidr" {
  description = "Nuestro Security Group"
  type        = string
  default     = "10.0.0.0/24"
}


variable "public_subnets" {
  description = "subnet con acceso a internet"
  type        = string
  default     = "10.0.0.128/26"
}


variable "private_subnets" {
  description = "subnet sin acceso a internet"
  type        = string
  default     = "10.0.0.192/26"
}
variable "ami_id" {
  description = "ID de la AMI de la instancia EC2"
  type        = string
  default     = "ami-080e1f13689e07408" # AMI de tu instancia EC2 (default AMI ID)
}


variable "instance_count" {
  type    = number
  default = 2  # Cambia este valor para ajustar la cantidad de instancias
}

variable "instance_name" {
  type    = string
  default = "intance_defoult"  # Prefijo del nombre de las instancias
}
